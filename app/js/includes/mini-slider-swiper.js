/**
 *  Mini Slider Swiper
 *
 **/
;(function (APP) {
	'use strict';

	var wrap = document.getElementById('mini-slider-swiper');
	var swiperContainer = wrap.querySelector('.swiper-container');

	var breakpoints = APP.Modules.BreakPoints;
	var bpSlider = {};

	bpSlider[breakpoints.max.sm] = {
		slidesPerView: 1
	};

	APP.Modules.MiniSliderSwiper = new Swiper(swiperContainer, {
		spaceBetween: 30,
		slidesPerView: 3,
		navigation: {
			prevEl: '.slider-btn-prev',
			nextEl: '.slider-btn-next'
		},
		breakpoints: bpSlider
	});
})(window.APP);