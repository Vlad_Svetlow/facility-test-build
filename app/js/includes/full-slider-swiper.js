/**
 *  Full Slider Swiper
 *
 **/
;(function (APP) {
	'use strict';
	
	var wrap = document.getElementById('full-slider-swiper');
	var swiperContainer = wrap.querySelector('.swiper-container');

	APP.Modules.FullSliderSwiper = new Swiper(swiperContainer, {
		pagination: {
			el: '.slider-pagination',
			clickable: true
		},
		navigation: {
			prevEl: '.slider-btn-prev',
			nextEl: '.slider-btn-next'
		},
		effect: 'fade',
		speed: 800,
		autoplay: {
			delay: 4500,
			disableOnInteraction: false
		}
	});
})(window.APP);