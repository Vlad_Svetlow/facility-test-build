/**
 *  Google Map
 *
 **/

;(function (APP) {
	'use strict';

	APP.Modules.GoogleMap = (function () {
		var self = {};

		var settings = {
			wrap: 'map',
			cords: {
				lat: 50.4501,
				lng: 30.5234
			},
			marker: {
				url: 'img/google-map/logo-map.svg'
			}
		};

		var mapConfig = {
			zoom: 12,
			center: settings.cords,
			scrollwheel: false
		};

		var wrap = document.getElementById(settings.wrap);

		self.init = function () {
			self.map = new google.maps.Map(wrap, mapConfig);

			google.maps.event.addDomListener(window, 'resize', function() {
				var center = self.map.getCenter();

				google.maps.event.trigger(self.map, "resize");
				self.map.setCenter(center);
			});
		};

		return self;
	})();
})(window.APP);