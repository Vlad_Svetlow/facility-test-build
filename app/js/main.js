/**
 *  Pre Init APP
 *
 **/

APP = window.APP || {};
APP.Plugins = APP.Plugins || {};
APP.Constructors = APP.Constructors || {};
APP.Modules = APP.Modules || {};

/**
 *  Helper Functions
 *
 **/

// Document ready func
function ready(fn) {
	if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
		fn();
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
}

// Extend method "extend({}, objA, objB)"
function extend(out) {
	out = out || {};

	for (var i = 1; i < arguments.length; i++) {
		if (!arguments[i])
			continue;

		for (var key in arguments[i]) {
			if (arguments[i].hasOwnProperty(key))
				out[key] = arguments[i][key];
		}
	}

	return out;
}

// Closest method
(function(ELEMENT) {
	ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
	ELEMENT.closest = ELEMENT.closest || function closest(selector) {
		if (!this) return null;
		if (this.matches(selector)) return this;
		if (!this.parentElement) {return null}
		else return this.parentElement.closest(selector)
	};
}(Element.prototype));

/**
 *  Toggle Overlay
 *
 **/

;(function (APP) {
	'use strict';

	APP.Constructors.ToggleMainNav = function (options) {
		var defaults = {
			triggerBtn: document.getElementById('triggerMenu'),
			overlay: document.querySelector('div.menu-overlay'),
			prefix: 'menu-overlay'
		};

		var settings = extend(defaults, options);

		var body = document.querySelector('body');

		var transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		};

		var transEndEventName = transEndEventNames[Modernizr.prefixed('transition')];
		var support = { transitions : Modernizr.csstransitions };

		var modName = {
			open: settings.prefix + '-open',
			close: settings.prefix + '-close'
		};

		this.toggleOverlay = function() {
			if( body.classList.contains(modName.open) ) {

				body.classList.remove(modName.open);
				body.classList.add(modName.close);

				var onEndTransitionFn = function( ev ) {
					if( support.transitions ) {
						if( ev.propertyName !== 'visibility' ) return;
						this.removeEventListener(transEndEventName, onEndTransitionFn);
					}
					body.classList.remove(modName.close);
				};
				if( support.transitions ) {
					settings.overlay.addEventListener(transEndEventName, onEndTransitionFn);
				}
				else {
					onEndTransitionFn();
				}
			}
			else if( !body.classList.contains(modName.close) ) {
				body.classList.add(modName.open);
			}
		};

		this.init = function () {
			settings.triggerBtn.addEventListener('click', this.toggleOverlay);
		};
	};
})(window.APP);
/**
 *  Scroll Anchors
 *
 **/

;(function (APP) {
	'use strict';

	APP.Constructors.ScrollAnchors = function (options) {
		var self = this;

		var defaults = {
			wrap: false,
			anchors: document.querySelectorAll('.scroll-anchor'),
			anchorWrap: false,
			middle: false
		};

		var settings = extend(defaults, options);

		var toggleItems = settings.anchorWrap ? settings.anchorWrap : settings.anchors;
		var sections = [];
		var hrefArr = [];
		var toggleAnchrosDisable = false;
		var isLastItemFullH;
		var currentIndex;
		var sizes;

		var nodeListThrough = function (node, cb) {
			for (var i = 0; i < node.length; i++) {
				cb(node[i], i);
			}
		};

		var clearAnchorActive = function (items) {
			nodeListThrough(items, function (el) {
				el.classList.remove('active');
			});
		};

		var setActiveAnchor = function (active, items) {
			// Exit if Multiply Click or 
			if (active.classList.contains('active') || active.classList.contains('disabled')) return;
			
			// Remove Active Class All
			clearAnchorActive(items);

			// Add Active Class Current
			active.classList.add('active');
		};

		var toggleAnchorClass = function (sections, index) {
			if (sections[index].name) {
				setActiveAnchor(toggleItems[index], toggleItems);
			} else {
				clearAnchorActive(toggleItems);
			}
		};

		var getScrollTop = function () {
			return window.pageYOffset || document.documentElement.scrollTop;
		};

		var getDocHeight = function () {
			var body = document.body,
				html = document.documentElement;

			return Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );
		};

		var getSizes = function () {
			var obj = {};
			obj.middle = {};

			obj.winH = window.innerHeight;
			obj.docH = getDocHeight();
			obj.lastSt = getScrollTop();

			obj.wrapH = settings.wrap ? settings.wrap.offsetHeight : 0;

			obj.middle.middle = Math.floor((obj.winH - obj.wrapH) / 2);
			obj.middle.top = Math.floor(obj.middle.middle / 2);
			obj.middle.bottom = obj.middle.top + obj.middle.middle;

			obj.stShift = settings.middle ? obj.wrapH + obj.middle[settings.middle] : obj.wrapH;
			obj.lastStTarget = obj.lastSt + obj.stShift;
			
			return obj;
		};

		var getSectionOffset = function (el) {
			var box = el.getBoundingClientRect();
			var offsetYTop = Math.floor(box.top + pageYOffset);
			var offsetYBottom = Math.floor(offsetYTop + el.offsetHeight);

			return {
				top: offsetYTop,
				bottom: offsetYBottom
			}
		};

		var getTargetByAnchor = function (el) {
			var name = el.getAttribute('href');
			name = name.split('#')[1];

			var element = document.getElementById(name);

			return {
				name: name,
				el: element
			};
		};

		var isScrollEnd = function (st, sizes) {
			return Math.floor(st) + sizes.winH >= sizes.docH;
		};

		var isCurrentTarget = function (st, offset) {
			return (st > offset.top && st < offset.bottom) || (st === 0 && offset.top === 0);
		};
		
		var isFullHeight = function (offset, winH) {
			return offset.bottom - offset.top - winH >= -1; // +- 1px for more compatible
		};

		var toggleLastItemActive = function (st, sizes) {
			if (!isLastItemFullH && isScrollEnd(st, sizes)) {

				currentIndex = sections.length - 1;

				if (toggleAnchrosDisable) return;

				if (sections[currentIndex].name) {
					clearAnchorActive(toggleItems);
					toggleItems[currentIndex].classList.add('active');
				} else {
					clearAnchorActive(toggleItems);
				}
			}
		};

		var hasPrevSiblings = function (offset, lastOffset) {
			var top = offset.top;
			var bottomL = lastOffset ? lastOffset.bottom : false;

			return bottomL ? Math.abs(top - bottomL) > 1 : top > 0;
		};

		var hasLastSiblings = function (offset, anchorIndex, anchors) {
			var anchorLength = anchors.length;
			var nextAnchor = anchors[anchorIndex + 1];
			var bottom = offset.bottom;

			var nextTarget = typeof nextAnchor === 'undefined' ? false : getTargetByAnchor(nextAnchor);
			var nextTargetEl = nextTarget ? nextTarget.el != null : false;
			var isLastAnchor = false;

			if (nextTarget) {
				isLastAnchor = anchorIndex + 1 === anchorLength - 1;
			} else {
				isLastAnchor = anchorIndex === anchorLength - 1;
			}

			return bottom + 1 < sizes.docH && !nextTargetEl && isLastAnchor;
		};

		var getSectionSet = function (el, index, anchors, prevSection, st) {
			var target = getTargetByAnchor(el);
			var lastOffset = typeof prevSection === 'undefined' ? false : prevSection.offset;
			var items = [];
			var activeIndex = -1;

			if (target.el === null) return false;

			var offset = getSectionOffset(target.el);
			var prevDisableSec = hasPrevSiblings(offset, lastOffset);
			var lastDisableSec = hasLastSiblings(offset, index, anchors);

			var data = {
				prev: {
					name: false,
					offset: {
						top: lastOffset ?  lastOffset.bottom : 0,
						bottom: offset.top
					}
				},
				active:{
					name: target.name,
					offset: offset
				},
				next: {
					name: false,
					offset: {
						top: offset.bottom,
						bottom: sizes.docH
					}
				}
			}; // consist of 3 obj items (prev disable section, active section, next disable section);

			// Check if has Prev Disabled Section
			if (prevDisableSec) {
				items.push(data.prev);

				if (isCurrentTarget(st, data.prev.offset)) {
					activeIndex = 0;
				}
			}

			items.push(data.active);

			if (isCurrentTarget(st, data.active.offset)) {
				activeIndex = prevDisableSec ? 1 : 0;
			}


			// Check if has Last Next Disabled Section
			if (lastDisableSec) {
				items.push(data.next);

				if (isCurrentTarget(st, data.next.offset)) {
					activeIndex = prevDisableSec ? 2 : 1;
				}
			}

			return {
				sections: items,
				activeSection: data.active,
				currentIndex: activeIndex
			};
		};

		var getSections = function () {
			var anchors = settings.anchors;
			var scrollTop = sizes.lastSt;
			var scrollTopTarget = sizes.lastStTarget;
			var res = [];
			var prevSection;

			// Reset Urls on Update
			if (hrefArr.length > 0) hrefArr = [];

			[].forEach.call(anchors, function (el, index) {
				var sectionSet = getSectionSet(el, index, anchors, prevSection, scrollTopTarget);

				if (!sectionSet) {
					hrefArr.push(false);
					toggleItems[index].classList.add('disabled');

					return;
				} else {
					var href = anchors[index].getAttribute('href');
					href = href.split('#')[1];
					
					hrefArr.push(href);
				}

				if (sectionSet.currentIndex > -1) {
					currentIndex = res.length + sectionSet.currentIndex;
				}

				res = res.concat(sectionSet.sections);
				prevSection = sectionSet.activeSection;

				// Check if Loop End
				if (index === anchors.length - 1) {
					var lastSection = res[res.length - 1];

					isLastItemFullH = isFullHeight(lastSection.offset, sizes.winH);

					toggleAnchorClass(res, currentIndex);
					toggleLastItemActive(scrollTop, sizes);
				}
			});

			return res;
		};

		var events = function () {
			var anchors = settings.anchors;
			var lastScrollTop = 0;

			// Window Scroll
			window.addEventListener('scroll', function () {
				var scrollTop = getScrollTop();
				var scrollTopTarget = scrollTop + sizes.stShift;
				var offset = sections[currentIndex].offset;

				if (scrollTop >= lastScrollTop) {
					// scroll down

					if (scrollTopTarget > offset.bottom) {
						currentIndex = currentIndex + 1;

						if (toggleAnchrosDisable) return;
						toggleAnchorClass(sections, currentIndex);
					}
				} else {
					// scroll up

					if (scrollTopTarget < offset.top) {
						currentIndex = currentIndex - 1;

						if (toggleAnchrosDisable) return;
						toggleAnchorClass(sections, currentIndex);
					}
				}
				
				// Check if Last - 1 Item
				if (currentIndex >= sections.length - 2) {
					toggleLastItemActive(scrollTop, sizes);
				}

				lastScrollTop = scrollTop;
			});

			// Window Resize
			window.addEventListener('resize', function () {
				self.update();
			});
			
			// Anchors Click
			[].forEach.call(anchors, function (el, index) {
				el.addEventListener('click', function (ev) {
					var currentItem = toggleItems[index];

					if (!hrefArr[index] || currentItem.classList.contains('active')) return;

					setActiveAnchor(currentItem, toggleItems);
				});
			});
		};

		this.init = function () {
			sizes = getSizes();
			sections = getSections();

			events();
		};

		this.getSections = function () {
			return sections;
		};

		this.getUrls = function () {
			return hrefArr;
		};

		this.disableToggleAnchors = function () {
			toggleAnchrosDisable = true;
		};

		this.enableToggleAnchors = function () {
			toggleAnchrosDisable = false;
		};

		this.updateSizes = function () {
			sizes = getSizes();

			return this;
		};

		this.updateSections = function () {
			sections = getSections();

			return this;
		};

		this.update = function () {
			this.updateSizes();
			this.updateSections();

			return this;
		};
	};
})(window.APP);
/**
 *  Toggle Scroll
 *
 **/
;(function (APP) {
	'use strict';

	APP.Plugins.ToggleScroll = (function () {
		var self = {};

		// left: 37, up: 38, right: 39, down: 40,
		// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
		var keys = [37, 38, 39, 40];

		function preventDefault(e) {
			e = e || window.event;
			if (e.preventDefault)
				e.preventDefault();
			e.returnValue = false;
		}

		function keydown(e) {
			for (var i = keys.length; i--;) {
				if (e.keyCode === keys[i]) {
					preventDefault(e);
					return;
				}
			}
		}

		function wheel(e) {
			preventDefault(e);
		}

		self.disable = function () {
			if (window.addEventListener) {
				window.addEventListener('DOMMouseScroll', wheel, false);
			}
			window.onmousewheel = document.onmousewheel = wheel;
			document.onkeydown = keydown;
		};

		self.enable = function () {
			if (window.removeEventListener) {
				window.removeEventListener('DOMMouseScroll', wheel, false);
			}
			window.onmousewheel = document.onmousewheel = document.onkeydown = null;
		};

		return self;
	})();

})(window.APP);
/**
 *  Loading Image
 *
 **/
;(function (APP) {
	'use strict';

	APP.Plugins.LoadingImage = function (src, render) {
		var img = new Image();

		function renderStarted() {
			requestAnimationFrame(render);
		}

		img.onload = function () {
			requestAnimationFrame(renderStarted);
		};

		img.src = src;

		return img;
	};
})(window.APP);
/**
 *  Main App
 *
 **/

;(function (APP) {
	'use strict';

	// Break Points Map
	APP.Modules.BreakPoints = {
		xs:  480,
		sm:  768,
		md:  992,
		lg:  1200,
		xlg: 1600
	};

	var breakpoints = APP.Modules.BreakPoints;

	APP.Modules.BreakPoints.max = {
		xs: breakpoints.xs - 1,
		sm: breakpoints.sm - 1,
		md: breakpoints.md - 1,
		lg: breakpoints.lg - 1,
		xlg: breakpoints.xlg - 1
	};

	// Document Ready
	ready(
		function () {
			// Init Main Nav Toggle Overlay
			APP.Modules.MainNavToggle = new APP.Constructors.ToggleMainNav({
				triggerBtn: document.getElementById('toggle-main-nav-btn'),
				overlay: document.getElementById('main-nav').querySelector('.toggle-overlay'),
				prefix: 'main-nav'
			});

			APP.Modules.MainNavToggle.init();


			// Init Header Anchors
			var mainHeader = document.getElementById('main-header');
			var mainNav = document.getElementById('main-nav');
			var mainNavItems = mainNav.querySelectorAll('.scroll-anchor');
			var mainNavItemsWrap = mainNav.querySelectorAll('.scroll-anchor-wrap');

			APP.Modules.HeaderAnchors = new APP.Constructors.ScrollAnchors({
				wrap: mainHeader,
				anchors: mainNavItems,
				anchorWrap: mainNavItemsWrap
			});

			APP.Modules.HeaderAnchors.init();

			// Detect Images Loaded and Update Header Anchors
			APP.Modules.ImagesLoaded = (function () {
				var exclude = ['#map'];
				var imagesAll = document.querySelectorAll('img');
				var selectImages = [];
				var i = 0;

				var loadImageLoop = function (images) {
					var loadIndex = 0;

					for (var t = 0; t < images.length; t++) {
						(function () {
							var img = images[t];

							return APP.Plugins.LoadingImage(img.src, function () {
								APP.Modules.HeaderAnchors.update();

								loadIndex++;
							});
						})();
					}
				};

				return Array.prototype.filter.call(imagesAll, function (img) {
					var ii;

					for (ii = exclude.length; ii--;) {
						if (img.closest(exclude[ii]) !== null) return;
					}

					selectImages.push(img);

					// Check Loop End
					if (imagesAll.length === ++i) {
						loadImageLoop(selectImages);
					}

					return img;
				});
			})();


			// Init Header Anchors Scroll To
			var mainNavScrollTo = new SmoothScroll('#main-header .scroll-anchor', {
				header: '#main-header',
				speed: 500,
				easing: 'easeInOutCubic',
				before: function () {
					APP.Modules.HeaderAnchors.disableToggleAnchors();
				},
				after: function () {
					APP.Modules.HeaderAnchors.enableToggleAnchors();
				}
			});
		}
	);
})(window.APP);
//# sourceMappingURL=map/main.js.map
